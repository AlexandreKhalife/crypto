import pandas as pd


def column_to_float(df, column):
    if column not in df.columns:
        df[column] = None
        return df
    df[column] = df[column].values.astype(str)
    had_percent_sign = df[column].str.contains("%")
    df.loc[:, column] = (
        df[column]
        .str.replace(" ", "")  # nbsp (%A0). Needed for some CSV files
        .str.replace(" ", "")  # normal space (%20)
        .str.replace("%", "")
        .str.replace(",", ".")
    )
    df.loc[:, column] = pd.to_numeric(df[column], errors="coerce")
    df[column] = df[column].values.astype(float)
    df.loc[had_percent_sign, column] = df[column] / 100
    return df
