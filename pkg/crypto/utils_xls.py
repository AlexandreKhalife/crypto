import xlrd
import datetime as dt


def xls_date_to_py_date(date_xls):
    """
    Excel date is a int or float
    To catch only excel date check if < 73051 (01/01/2100)
    This allow to exclude date sawn as int such as 01122022
    ============
    unit testing
    date = 44596
    date = 44596.4965640046
    """
    if type(date_xls) is int or type(date_xls) is float and date_xls < 73051:
        date = xlrd.xldate_as_tuple(date_xls, 0)
        date = dt.datetime(*date)
        return date
    else:
        return "not a date"


def write_df_to_excel(df, wb, sheet, cell="A1"):
    sheet = wb.sheets[sheet]
    sheet.clear_contents()
    sheet.range(cell).options(index=False).value = df
