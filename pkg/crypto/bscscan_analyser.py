import pandas as pd
import numpy as np
from crypto.utils_df import column_to_float


def clean_token_transaction_df(df, address):
    columns_float = ["gasPrice", "gasUsed", "value"]
    for column in columns_float:
        df = column_to_float(df, column)
    columns_10e18 = ["gasPrice", "value"]
    for column in columns_10e18:
        df.loc[:, column] = df[column] * 10e-19
    df.loc[:, "txnFeeBNB"] = df["gasPrice"] * df["gasUsed"]
    address = address.lower()
    df.loc[:, "InOut"] = np.where(
            df["from"] == address,
            "OUT",
            "IN"
    )
    df.loc[:, "net_value"] = np.where(
        df["InOut"] == "IN",
        df["value"],
        -df["value"]
    )
    return df


def calculate_token_balance(df):
    key_columns = ["contractAddress"]
    value_columns = [
            "InOut",
            "tokenName",
            "tokenSymbol",
            "tokenDecimal",
            "net_value",
            "txnFeeBNB"
    ]
    aggfunc = {
            "net_value": "sum",
            "txnFeeBNB": "sum",
            "tokenName": "first",
            "tokenSymbol": "first",
            "tokenDecimal": "first"
    }
    df_pivot = df[[*key_columns, *value_columns]]
    pivot = df_pivot.groupby(key_columns)[value_columns].agg(aggfunc)
    df_result = pd.DataFrame(pivot.to_records())
    df_result = df_result[df_result["net_value"] >= 1e-19]
    return df_result
