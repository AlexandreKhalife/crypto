from setuptools import find_packages
from setuptools import setup

requirements = """
pandas==1.3.4
xlrd==1.2.0
flake8==4.0.1
"""

setup(
    name='crypto',
    setup_requires=['setuptools_scm'],
    use_scm_version={
        "write_to": "../version.txt",
        "root": "..",
        "relative_to": __file__,
    },
    packages=find_packages(),
    install_requires=requirements,
    include_package_data=True,
    author="Alexandre Khalifé",
    zip_safe=False)
