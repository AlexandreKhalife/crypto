from setuptools import find_packages
from setuptools import setup

requirements = """
requests==2.22.0
"""

setup(
    name="crypto_api",
    setup_requires=["setuptools_scm"],
    use_scm_version={
        "write_to": "../version.txt",
        "root": "..",
        "relative_to": __file__,
    },
    packages=find_packages(),
    install_requires=requirements,
    include_package_data=True,
    author="Alexandre Khalifé",
    zip_safe=False)
