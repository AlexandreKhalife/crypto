import requests


def request_api(api_url, headers=None, params=None):
    try:
        response = requests.get(api_url, headers=headers, params=params)
    except requests.exceptions.ConnectionError:
        return {"error": "Network Error"}
    except requests.exceptions.RequestException:
        return {"error": "Error"}
    return response
