"""
Documentation: https://binance-docs.github.io/apidocs/spot/en/#introduction
"""

import hmac
import time
from datetime import (
        datetime,
        timedelta
)
import hashlib
import pandas as pd
import yaml
from urllib.parse import urlencode
from crypto_api.external_api.secret import BINANCE_API_KEY
from crypto_api.external_api.secret import BINANCE_SECRET_KEY
from crypto_api.utils_api import request_api
from crypto.utils_df import column_to_float


def sign_request(secret, params=None):
    if params is None:
        params = {}
    params["timestamp"] = get_timestamp()
    query_string = prepare_params(params)
    signature = sign_data(secret, query_string)
    params["signature"] = signature
    return params


def get_timestamp():
    return int(time.time() * 1000)


def prepare_params(d) -> dict:
    out = {}
    for k in d.keys():
        if d[k] is not None:
            out[k] = d[k]
    out = urlencode(out, True).replace("%40", "@")
    return out


def sign_data(secret, data):
    m = hmac.new(secret.encode("utf-8"), data.encode("utf-8"), hashlib.sha256)
    return m.hexdigest()


def get_savings_balance():
    url = "https://api.binance.com"
    endpoint = "/sapi/v1/lending/union/account"
    api_url = f"{url}{endpoint}"
    headers = {"X-MBX-APIKEY": BINANCE_API_KEY}
    params = {}
    params = sign_request(BINANCE_SECRET_KEY, params)
    response = request_api(api_url, headers=headers, params=params)
    df = pd.json_normalize(response.json()["positionAmountVos"])
    columns_float = ["amount", "amountInBTC", "amountInUSDT"]
    for column in columns_float:
        df = column_to_float(df, column)
    return df[df["amount"] != 0]


def get_spot_balance():
    url = "https://api.binance.com"
    endpoint = "/api/v3/account"
    api_url = f"{url}{endpoint}"
    headers = {"X-MBX-APIKEY": BINANCE_API_KEY}
    params = {}
    params = sign_request(BINANCE_SECRET_KEY, params)
    response = request_api(api_url, headers=headers, params=params)
    df = pd.json_normalize(response.json()["balances"])
    columns_float = ["free", "locked"]
    for column in columns_float:
        df = column_to_float(df, column)
    df.loc[:, "amount"] = df["free"] + df["locked"]
    return df[df["amount"] != 0]


def get_flexible_product_list():
    url = "https://api.binance.com"
    endpoint = "/sapi/v1/lending/daily/product/list"
    api_url = f"{url}{endpoint}"
    headers = {"X-MBX-APIKEY": BINANCE_API_KEY}
    response = []
    response_page = {}
    page = 1
    while "error" not in response_page:
        params = {
                "status": "SUBSCRIBABLE",
                "size": 100,
                "current": page
                }
        params = sign_request(BINANCE_SECRET_KEY, params)
        response_page = request_api(api_url, headers=headers, params=params)
        if "error" in response_page:
            break
        else:
            response += response_page.json()
        page += 1
    df = pd.json_normalize(response)
    usefull_cols = [
            "productId", "asset", "status", "canPurchase", "canRedeem",
            "featured", "avgAnnualInterestRate", "upLimitPerUser"
            ]
    df = df[usefull_cols]
    columns_float = ["avgAnnualInterestRate", "upLimitPerUser"]
    for column in columns_float:
        df = column_to_float(df, column)
    df.sort_values(by=["avgAnnualInterestRate"], ascending=False)
    return df


def get_all_withdraw():
    url = "https://api.binance.com"
    endpoint = "/sapi/v1/capital/withdraw/history"
    api_url = f"{url}{endpoint}"
    headers = {"X-MBX-APIKEY": BINANCE_API_KEY}
    end_date = datetime.now()
    response = []
    response_page = {}
    while "error" not in response_page:
        # For some reason... 1h gap
        end_date = end_date + timedelta(minutes=60)
        end_time = int(time.mktime(end_date.timetuple()) * 1000)
        start_date = end_date - timedelta(days=90)
        start_time = int(time.mktime(start_date.timetuple()) * 1000)
        params = {
                "status": "6",
                "startTime": start_time,
                "endTime": end_time,
                }
        params = sign_request(BINANCE_SECRET_KEY, params)
        response_page = request_api(api_url, headers=headers, params=params)
        if "error" in response_page:
            break
        else:
            response += response_page.json()
            oldest_date = min([x["applyTime"] for x in response_page.json()])
            oldest_date = datetime.strptime(oldest_date, "%Y-%m-%d %H:%M:%S")
            end_date = oldest_date - timedelta(milliseconds=1)
    df = pd.json_normalize(response)
    return df


def get_all_deposit():
    url = "https://api.binance.com"
    endpoint = "/sapi/v1/capital/deposit/hisrec"
    api_url = f"{url}{endpoint}"
    headers = {"X-MBX-APIKEY": BINANCE_API_KEY}
    end_date = datetime.now()
    response = []
    response_page = {}
    while "error" not in response_page:
        # for some reason, no hour gap
        end_time = int(time.mktime(end_date.timetuple()) * 1000)
        start_date = end_date - timedelta(days=90)
        start_time = int(time.mktime(start_date.timetuple()) * 1000)
        params = {
                "status": "1",
                "startTime": start_time,
                "endTime": end_time,
                }
        params = sign_request(BINANCE_SECRET_KEY, params)
        response_page = request_api(api_url, headers=headers, params=params)
        if "error" in response_page:
            break
        else:
            response += response_page.json()
            oldest_date = min([x["insertTime"] for x in response_page.json()])
            oldest_date = datetime.fromtimestamp(oldest_date/1000)
            end_date = oldest_date - timedelta(milliseconds=1)
    df = pd.json_normalize(response)
    return df


def get_existing_tradings_pairs():
    url = "https://api.binance.com"
    endpoint = "/api/v3/exchangeInfo"
    api_url = f"{url}{endpoint}"
    response = request_api(api_url)
    symbols = [key["symbol"] for key in response.json()["symbols"]]
    return symbols


def get_my_tradings_pairs(filter_symbol=None):
    url = "https://api.binance.com"
    endpoint = "/api/v3/allOrders"
    api_url = f"{url}{endpoint}"
    headers = {"X-MBX-APIKEY": BINANCE_API_KEY}
    my_symbols = []
    symbols = get_existing_tradings_pairs()
    if filter_symbol is not None:
        symbols = [val for val in symbols if filter_symbol in val]
    for symbol in symbols:
        params = {
                "symbol": symbol,
                "orderId": "1",
                "limit": "1"
                }
        params = sign_request(BINANCE_SECRET_KEY, params)
        response = request_api(api_url, headers=headers, params=params)
        time.sleep(0.5)
        if "error" in response:
            print(f"{symbols.index(symbol)}/{len(symbols)}: {symbol} - None")
            continue
        if "code" in response.json():
            print(f"{symbols.index(symbol)}/{len(symbols)}: {symbol} - Error")
            print(response.json()["msg"])
            break
        if "error" not in response:
            my_symbols.append(symbol)
            print(f"{symbols.index(symbol)}/{len(symbols)}: {symbol} - OK")
    with open("my_symbols.yaml", "w") as file:
        yaml.dump(my_symbols, file)
    return my_symbols


def load_my_trading_pairs():
    try:
        with open("my_symbols.yaml", "r") as file:
            my_symbols = yaml.safe_load(file)
    except OSError:
        my_symbols = None
    return my_symbols


def get_all_trade():
    url = "https://api.binance.com"
    endpoint = "/api/v3/allOrders"
    api_url = f"{url}{endpoint}"
    headers = {"X-MBX-APIKEY": BINANCE_API_KEY}
    response = []
    my_symbols = load_my_trading_pairs()
    if my_symbols is None:
        return
    for symbol in my_symbols:
        print(f"{my_symbols.index(symbol)}/{len(my_symbols)}")
        params = {
                "symbol": symbol,
                "orderId": "1",
                "limit": "1000"
                }
        params = sign_request(BINANCE_SECRET_KEY, params)
        response_page = request_api(api_url, headers=headers, params=params)
        if "error" in response_page:
            print("error: ", symbol)
            continue
        if response_page.status_code == 200:
            print("success: ", symbol, len(response_page.json()))
            response += response_page.json()
        else:
            print(response_page.json())
            continue
    df = pd.json_normalize(response)
    df = df[df["status"] == "FILLED"]
    return df


def get_pair_info(symbol):
    url = "https://api.binance.com"
    endpoint = "/api/v3/exchangeInfo"
    api_url = f"{url}{endpoint}"
    params = {"symbol": symbol}
    response = request_api(api_url, params=params)
    response = response.json()
    base_asset = response["symbols"][0]["baseAsset"]
    quote_asset = response["symbols"][0]["quoteAsset"]
    return base_asset, quote_asset
