from crypto_api.utils_api import request_api
from crypto.utils_xls import xls_date_to_py_date
import datetime as dt


def capi_quote_coin_histo(symbol, date_to_quote, quote):
    date_to_quote = xls_date_to_py_date(date_to_quote)
    time_start = str(date_to_quote.isoformat())
    time_end = date_to_quote + dt.timedelta(days=1)
    time_end = str(time_end.isoformat())
    period_id = '1SEC'
    url = "https://rest.coinapi.io"
    endpoint = f"/v1/exchangerate/{symbol}/{quote}/history"
    api_url = f"{url}{endpoint}"
    headers = {'X-CoinAPI-Key': '6FE56506-A188-4D0D-B0DF-F399D8DFE928'}
    params = {
            'period_id': period_id,
            'time_start': time_start,
            'time_end': time_end,
            'limit': 1
            }
    response = request_api(api_url, headers, params)
    response = response.json()
    if "error" in response:
        return response["error"]
    else:
        return response[0]["rate_open"]
