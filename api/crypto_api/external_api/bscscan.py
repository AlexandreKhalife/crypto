from crypto_api.utils_api import request_api
import pandas as pd


def get_bnb_balance(address):
    url = "https://api.bscscan.com"
    endpoint = "/api"
    api_url = f"{url}{endpoint}"
    params = {
            "module": "account",
            "action": "balance",
            "address": address,
            "apikey": "MHMAC7HYD8N1NBS1IABCZ9QA18YA3UQHK8i"}
    response = request_api(api_url, params=params)
    response = response.json()
    if "error" in response:
        return response["error"]
    else:
        bnb_balance = float(response["result"])*10e-19
        return bnb_balance


def get_token_balance(address, contractaddress):
    url = "https://api.bscscan.com"
    endpoint = "/api"
    api_url = f"{url}{endpoint}"
    params = {
            "module": "account",
            "action": "tokenbalance",
            "contractaddress": contractaddress,
            "address": address,
            "apikey": "MHMAC7HYD8N1NBS1IABCZ9QA18YA3UQHK8i"}
    response = request_api(api_url, params=params)
    response = response.json()
    if "error" in response:
        return response["error"]
    else:
        token_balance = float(response["result"])*10e-19
        return token_balance


def get_normal_transactions(address):
    url = "https://api.bscscan.com"
    endpoint = "/api"
    api_url = f"{url}{endpoint}"
    params = {
            "module": "account",
            "action": "txlist",
            "address": address,
            "apikey": "MHMAC7HYD8N1NBS1IABCZ9QA18YA3UQHK8i"}
    response = request_api(api_url, params=params)
    response = response.json()
    if "error" in response:
        return response["error"]
    else:
        df = pd.json_normalize(response["result"])
        return df


def get_token_transactions(address):
    url = "https://api.bscscan.com"
    endpoint = "/api"
    api_url = f"{url}{endpoint}"
    params = {
            "module": "account",
            "action": "tokentx",
            "address": address,
            "apikey": "MHMAC7HYD8N1NBS1IABCZ9QA18YA3UQHK8i"}
    response = request_api(api_url, params=params)
    response = response.json()
    if "error" in response:
        return response["error"]
    else:
        df = pd.json_normalize(response["result"])
        return df


def get_abi_contract(contractaddress):
    url = "https://api.bscscan.com"
    endpoint = "/api"
    api_url = f"{url}{endpoint}"
    params = {
            "module": "contract",
            "action": "getabi",
            "address": contractaddress,
            "apikey": "MHMAC7HYD8N1NBS1IABCZ9QA18YA3UQHK8i"}
    response = request_api(api_url, params=params)
    if "error" in response:
        return response["error"]
    else:
        return response


def get_all_token_balance_from_address(address):
    """
    pro
    """
    url = "https://api.bscscan.com"
    endpoint = "/api"
    api_url = f"{url}{endpoint}"
    params = {
            "module": "token",
            "action": "tokenholderlist",
            "address": address,
            "apikey": "MHMAC7HYD8N1NBS1IABCZ9QA18YA3UQHK8i"}
    response = request_api(api_url, params=params)
    response = response.json()
    if "error" in response:
        return response["error"]
    else:
        return response
