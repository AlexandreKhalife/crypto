from crypto_api.utils_api import request_api
import pandas as pd


def col_to_percentage(df, col_list):
    for col in col_list:
        df.loc[:, col] = df[col] / 100
    return df


def cmc_quote_all_latest():
    url = "https://pro-api.coinmarketcap.com"
    endpoint = "/v1/cryptocurrency/listings/latest"
    api_url = f"{url}{endpoint}"
    headers = {"X-CMC_PRO_API_KEY": "a25516dc-b52c-4781-9441-e35166571bff"}
    response = request_api(api_url, headers)
    response = response.json()
    df = pd.json_normalize(response["data"])
    df = df[[
        "id",
        "cmc_rank",
        "symbol",
        "name",
        "quote.USD.price",
        "quote.USD.volume_24h",
        "quote.USD.percent_change_24h",
        "quote.USD.percent_change_7d",
        "quote.USD.percent_change_30d",
        "quote.USD.percent_change_60d",
        "quote.USD.percent_change_90d",
        "quote.USD.market_cap",
        "circulating_supply",
        "total_supply",
        "max_supply",
        "quote.USD.last_updated"
        ]]
    list_col_to_percentage = [
        "quote.USD.percent_change_24h",
        "quote.USD.percent_change_7d",
        "quote.USD.percent_change_30d",
        "quote.USD.percent_change_60d",
        "quote.USD.percent_change_90d"
        ]
    df = col_to_percentage(df, list_col_to_percentage)
    return df


def cmc_quote_coin_latest(symbol):
    url = "https://pro-api.coinmarketcap.com"
    endpoint = "/v1/cryptocurrency/quotes/latest"
    api_url = f"{url}{endpoint}"
    headers = {"X-CMC_PRO_API_KEY": "a25516dc-b52c-4781-9441-e35166571bff"}
    params = {'symbol': symbol}
    response = request_api(api_url, headers, params)
    response = response.json()
    if "error" in response:
        return response["error"]
    else:
        price_usd = response["data"][symbol]["quote"]["USD"]["price"]
        return price_usd
