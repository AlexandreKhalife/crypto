clean:
	@find . -name '__pycache__' |xargs rm -fr {} \;
	@find . -type d -name '*.egg-info' |xargs rm -fr {} \;
	@find . -type d -name '*.eggs' |xargs rm -fr {} \;
dev_install_full:
	@cd pkg && make dev_install
	@cd api && make dev_install
install_full:
	@cd pkg && make install
	@cd api && make install
qa_check_code:
	@find ./pkg -name "*.py" | xargs flake8 --exclude=*__init__.py,*eggs*
	@find ./api -name "*.py" | xargs flake8 --exclude *.eggs*
	@find ./xlwings -name "*.py" | xargs flake8 --exclude *.eggs*
