# crypto


## Installation


### Option 1: Windows + Excel

Usage: 
- Excel usage from Windows

#### Minimal requirements

You need to have the following install on Windows:
- Python3 [with `add to path == True`] (https://www.python.org/downloads/windows/)

#### Installation steps

From **Windows Powershell**:

1. Install xlwings & xlwings add-in (https://docs.xlwings.org/en/stable/installation.html)
   ```
   $ pip install xlwings
   $ xlwings addin install
   ```
2. Clone the crypto repo
   ```
   $ git clone git@gitlab.com:AlexandreKhalife/crypto.git
   ```
4. Install the crypto package
   ```
   $ cd Path_To_Repo/crypto 
   $ pip install -e pkg/.
   $ pip install -e api/.
   ```


### Option 2: Linux + CLI

None


## Configure your Excel xlwings

### Step 1: Get a Excel working with xlwings

Download last version of `wallet_analyzer.xlsm` from: `<link comming soon>`

OR

Initiate an empty Excel with minimal xlwings with Windows PowerShell
   ```
   $ xlwings quickstart myproject

   ```

### Step 2: Configure the sheet "xlwings.conf"
- `PYTHONPATH`: Path_To_Repo/crypto/xlwings
- `UDF Modules`: xls_crypto;xls_standard

### Step 3: Enable Trust access to VBA project

Enable Trust access to the VBA project object model under File > Options > Trust Center > Trust Center Settings > Macro Settings. You only need to do this once. Also, this is only required for importing the functions, i.e. end users won’t need to bother about this


