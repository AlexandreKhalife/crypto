import xlwings as xw
from crypto_api.external_api.coinmarketcap import (
        cmc_quote_all_latest,
        cmc_quote_coin_latest
)
from crypto_api.external_api.coinapi import (
        capi_quote_coin_histo
)
from crypto_api.external_api.bscscan import (
        get_bnb_balance,
        get_token_transactions
)
from crypto.bscscan_analyser import (
        clean_token_transaction_df,
        calculate_token_balance
)
from crypto.utils_xls import (
        write_df_to_excel
)


def main():
    wb = xw.Book.caller()
    df_latest = cmc_quote_all_latest()
    # df_latest = pd.read_csv("latest.csv")
    write_df_to_excel(df_latest, wb, sheet="latest")


@xw.func
def GET_BSC_TOKEN_TRANSACTIONS(address):
    df = get_token_transactions(address)
    df = clean_token_transaction_df(df, address)
    return df


@xw.func
def GET_BSC_TOKEN_TRANSACTIONS_BALANCE(address):
    df = get_token_transactions(address)
    df = clean_token_transaction_df(df, address)
    df = calculate_token_balance(df)
    return df


@xw.func
def QUOTE_COIN_HISTORIC(symbol, date_to_quote, quote="USD"):
    coin_value = capi_quote_coin_histo(symbol, date_to_quote, quote)
    return coin_value


@xw.func
def QUOTE_COIN_LATEST(symbol):
    """
    warning: max 333 queries per day
    cost 1 query for each call
    so 1 query for each Excel formula/recalculation...
    better to call /list and then RECHERCHEX()
    """
    coin_value = cmc_quote_coin_latest(symbol)
    return coin_value


@xw.func
def GET_BNB_BALANCE_FROM_ADDRESS(address):
    bnb_balance = get_bnb_balance(address)
    return bnb_balance


if __name__ == "__main__":
    main()
