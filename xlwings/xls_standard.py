import xlwings as xw


@xw.func
@xw.arg('x', doc='This is x', numbers=int)
@xw.arg('y', doc='This is y', numbers=int)
def TEST_TEST(x, y):
    return x + y
